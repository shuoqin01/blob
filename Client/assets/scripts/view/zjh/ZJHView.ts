import ZJHCtroller from "../../controller/zjh/ZJHController";
import ZJHHeadList from "../../game/zjh/component/ZJHHeadList";
import ZJHBottomBtn from "../../game/zjh/component/ZJHBottomBtn";
import ZJHBehaviorList from "../../game/zjh/component/ZJHBehaviorList";
import ZBets from "../../game/zjh/ZBets";
import { Config, ZJHPlayer } from "../../game/zjh/ZJHRound";
import ZJHTop from "../../game/zjh/component/ZJHTop";
import ZJHVs from "../../game/zjh/component/ZJHVs";


/*
 * @Author: fasthro
 * @Description: 界面
 * @Date: 2019-04-03 15:07:45
 */

const { ccclass, property, menu } = cc._decorator;

@ccclass
@menu("炸金花/View")
export default class ZJHView extends cc.Component {
    // controller
    private _controller: ZJHCtroller;

    // 头像：头部信息
    @property(ZJHHeadList)
    public headList: ZJHHeadList = null;

    //行为：准备，翻牌等等
    @property(ZJHBehaviorList)
    public behaviorList: ZJHBehaviorList = null;

    //底部按钮: 比牌，看牌等等
    @property(ZJHBottomBtn)
    public bottomBtn: ZJHBottomBtn = null;

    //赌注图标：加注，跟注的图标显示地方
    @property(ZBets)
    public zBets: ZBets = null;

    //顶部： 显示单注，总注，返回按钮
    @property(ZJHTop)
    public zTop: ZJHTop = null;

    // 弹窗 node:显示要加倍的赌注
    @property(cc.Node)
    public popupNode: cc.Node = null;
    // 100
    @property(cc.Button)
    public oneBet: cc.Button = null;
    // 500
    @property(cc.Button)
    public fiveBet: cc.Button = null;
    // 1000
    @property(cc.Button)
    public tenBet: cc.Button = null;

    //比牌悬浮框
    @property(ZJHVs)
    public vsPopup: ZJHVs = null;

    onLoad() {
        this.headList = this.node.getChildByName("headList").getComponent(ZJHHeadList);
        this.behaviorList = this.node.getChildByName("behaviorList").getComponent(ZJHBehaviorList);
        this.bottomBtn = this.node.getChildByName("bottom").getComponent(ZJHBottomBtn);
        this.zBets = this.node.getChildByName("bets").getComponent(ZBets);
        this.popupNode = this.node.getChildByName("betPopup");
        let betLayout: cc.Node = this.popupNode.getChildByName("betLayout");
        this.oneBet = betLayout.getChildByName("game_chip01").getComponent(cc.Button);
        this.fiveBet = betLayout.getChildByName("game_chip03").getComponent(cc.Button);
        this.tenBet = betLayout.getChildByName("game_chip05").getComponent(cc.Button);
        this.zTop = this.node.getChildByName("top").getComponent(ZJHTop);
        this.vsPopup = this.node.getChildByName("vs_popup").getComponent(ZJHVs);

        //绑定点击事件
        this.behaviorList.behaviorV.readyBtn.getComponent(cc.Button).clickEvents.push(this.createClickEventHandler("_clickReady"));
        this.zTop.back.clickEvents.push(this.createClickEventHandler("_clickBack"));//返回
        this.bottomBtn.btnFlip.clickEvents.push(this.createClickEventHandler("_clickFlip"));//看牌
        this.bottomBtn.btnDrop.clickEvents.push(this.createClickEventHandler("_clickDrop"));//弃牌
        this.bottomBtn.btnFollow.clickEvents.push(this.createClickEventHandler("_clickFollow"));//跟注
        this.bottomBtn.btnAdd.clickEvents.push(this.createClickEventHandler("_clickAdd"));//加注
        this.bottomBtn.btnVs.clickEvents.push(this.createClickEventHandler("_clickVS"));//比牌

        this.oneBet.clickEvents.push(this.createClickEventHandler("_clickAddBet","0"));// 加注 100
        this.fiveBet.clickEvents.push(this.createClickEventHandler("_clickAddBet", "1"));//加注 500
        this.tenBet.clickEvents.push(this.createClickEventHandler("_clickAddBet", "2"));//加注 1000

        //玩家比牌
        this.behaviorList.behaviorX.vsIcon.getComponent(cc.Button).clickEvents.push(this.createClickEventHandler("_clickPickVs","1"));
        this.behaviorList.behaviorY.vsIcon.getComponent(cc.Button).clickEvents.push(this.createClickEventHandler("_clickPickVs","2"));
        this.behaviorList.behaviorZ.vsIcon.getComponent(cc.Button).clickEvents.push(this.createClickEventHandler("_clickPickVs","3"));
        this.behaviorList.behaviorW.vsIcon.getComponent(cc.Button).clickEvents.push(this.createClickEventHandler("_clickPickVs","4"));
    }

    /**
     * 初始化 view
     */
    public initView(controller: ZJHCtroller): void {
        this._controller = controller;

        this.headList.init(controller);

        this.behaviorList.init(controller);

        this.bottomBtn.init();

        this.zTop.init();

        this.zBets.init();
    }

    /**
     * 开始游戏
     */
    public startGame(betCoin: number, sum: number){
        this.headList.setHeadData();
        this.behaviorList.startGame(betCoin);
        this.setTop(betCoin, sum);
    }

    //设置顶部 赌注
    public setTop(betCoin: number, sum: number) {
        this.zTop.betUnit.string = betCoin.toString();
        this.zTop.betSum.string = sum.toString();
    }
    /**
     * 设置X为庄
     */
    public setCreateLordX(): void {
        this.headList.headX.setLord(true);
    }

    /**
     * 设置Y为庄
     */
    public setCreateLordY(): void {
        this.headList.headY.setLord(true);
    }

    /**
     * 设置Z为庄
     */
    public setCreateLordZ(): void {
        this.headList.headZ.setLord(true);
    }

    /**
     * 设置W为庄
     */
    public setCreateLordW(): void {
        this.headList.headW.setLord(true);
    }

    /**
     * 设置V为庄
     */
    public setCreateLordV(): void {
        this.headList.headV.setLord(true);
    }

    /**
     * 设置X倒计时
     * @param timeout 
     */
    public setTimeoutX(timeout: number): void {
        this.behaviorList.behaviorX.setCountdown(true, timeout);
    }

    /**
     * 设置Y倒计时
     * @param timeout 
     */
    public setTimeoutY(timeout: number): void {
        this.behaviorList.behaviorY.setCountdown(true, timeout);
    }

    /**
     * 设置Z倒计时
     * @param timeout 
     */
    public setTimeoutZ(timeout: number): void {
        this.behaviorList.behaviorZ.setCountdown(true, timeout);
    }
    /**
     * 设置W倒计时
     * @param timeout 
     */
    public setTimeoutW(timeout: number): void {
        this.behaviorList.behaviorW.setCountdown(true, timeout);
    }
    /**
     * 设置V倒计时
     * @param timeout 
     */
    public setTimeoutV(timeout: number): void {
        this.behaviorList.behaviorV.setCountdown(true, timeout);
    }

    /**
     * 创建点击事件
     * @param handler 
     * @param customEventData 
     */
    public createClickEventHandler(handler: string, customEventData: string = "") {
        let clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node;
        clickEventHandler.component = "ZJHView";
        clickEventHandler.handler = handler;
        clickEventHandler.customEventData = customEventData;
        return clickEventHandler
    }

    //点击准备
    private _clickReady(){
        this._controller.clickReady();
    }
    
    //点击看牌
    private _clickFlip(){
        cc.log("zjh==>看牌");
        this._controller.getPlayerV().flip();
    }

    //点击弃牌
    private _clickDrop(){
        cc.log("zjh==>弃牌");
        //显示弃牌标志, 修改状态
        this._controller.getPlayerV().drop();
    }

    //点击跟注
    private _clickFollow(){
        cc.log("zjh==>跟注");
        this._controller.getPlayerV().follow();
    }

    //点击加注
    private _clickAdd(){
        cc.log("zjh==>加注");
        this.bottomBtn.activeButtons(false, false);
        this.popupNode.active = true;
        this._controller.preAdd();
    }

    //点击添加的赌注
    private _clickAddBet(event, customEventData){
        this.bottomBtn.restoreState();
        let index = parseInt(customEventData);
        cc.log("click add bet ==> "+Config.BET_ARRAY[index],"index="+index);
        if(index == Config.BET_ARRAY.length-1){
            this.bottomBtn.activeAdd(false);
        }
        this.popupNode.active = false;
        this._controller.getPlayerV().add(index);
    }

    //点击比牌
    private _clickVS(){
        cc.log("zjh==>比牌");
        this._controller.preVs();
        this.bottomBtn.activeButtons(false, false);
        // 显示比牌图标: 根据在线玩家
        this.showVsIcon(true);
    }

    //选择比牌
    private _clickPickVs(event, customEventData){
        this.showVsIcon(false);
        this.bottomBtn.restoreState();
        let id = parseInt(customEventData);
        cc.log("选择比牌 ==> 玩家"+id);
        this._controller.getPlayerV().pickVs(id);

    }
    //点击返回
    private _clickBack(){
        cc.log("zjh==>返回");
    }

    //根据在线玩家 显示比牌图标
    private showVsIcon(active: boolean){
        let xState = this._controller.getPlayerX().state;
        let yState = this._controller.getPlayerY().state;
        let zState = this._controller.getPlayerZ().state;
        let wState = this._controller.getPlayerW().state;
        this.behaviorList.activeVs(active, xState, yState, zState, wState);
    }
}