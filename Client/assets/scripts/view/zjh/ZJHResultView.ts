import ZJHResultItemCom from "../../game/zjh/component/ZJHResultItemCom";
import { ZJHPlayer } from "../../game/zjh/ZJHRound";

/*
 * @Author: fasthro
 * @Description: 结果界面
 * @Date: 2019-04-08 17:44:58
 */

const { ccclass, property, menu } = cc._decorator;

@ccclass
@menu("炸金花/ResultView")
export default class ZJHResultView extends cc.Component {

    //玩家v
    @property(ZJHResultItemCom)
    public itemV: ZJHResultItemCom = null;

    //x
    @property(ZJHResultItemCom)
    public itemX: ZJHResultItemCom = null;

    //y
    @property(ZJHResultItemCom)
    public itemY: ZJHResultItemCom = null;
    //z
    @property(ZJHResultItemCom)
    public itemZ: ZJHResultItemCom = null;
    //w
    @property(ZJHResultItemCom)
    public itemW: ZJHResultItemCom = null;

    // win node
    @property(cc.Node)
    public winNode: cc.Node = null;

    // lose node
    @property(cc.Node)
    public loseNode: cc.Node = null;

    /**
     * 设置x信息
     * @param name 玩家昵称
     * 记得给赢家增加赌注
     */
    public setItemV(player: ZJHPlayer, allBet: number): void {
        this.itemV.setCards(false, player.cards);
        this.itemV.nameLabel.string = player.name;
        this.itemV.scoreLabel.string = player.state ? "赢": "输";
        this.itemV.betLabel.string = player.state ? "+"+allBet: "-"+player.sum;
    }
    /**
     * 设置x信息
     * @param name 玩家昵称
     * 记得给赢家增加赌注
     */
    public setItemX(player: ZJHPlayer, allBet: number): void {
        this.itemX.setCards(false, player.cards);
        this.itemX.nameLabel.string = player.name;
        this.itemX.scoreLabel.string = player.state ? "赢": "输";
        this.itemX.betLabel.string = player.state ? "+"+allBet: "-"+player.sum;
    }
    /**
     * 设置x信息
     * @param name 玩家昵称
     * 记得给赢家增加赌注
     */
    public setItemY(player: ZJHPlayer, allBet: number): void {
        this.itemY.setCards(false, player.cards);
        this.itemY.nameLabel.string = player.name;
        this.itemY.scoreLabel.string = player.state ? "赢": "输";
        this.itemY.betLabel.string = player.state ? "+"+allBet: "-"+player.sum;
    }
    /**
     * 设置x信息
     * @param name 玩家昵称
     * 记得给赢家增加赌注
     */
    public setItemZ(player: ZJHPlayer, allBet: number): void {
        this.itemZ.setCards(false, player.cards);
        this.itemZ.nameLabel.string = player.name;
        this.itemZ.scoreLabel.string = player.state ? "赢": "输";
        this.itemZ.betLabel.string = player.state ? "+"+allBet: "-"+player.sum;
    }
    /**
     * 设置x信息
     * @param name 玩家昵称
     * 记得给赢家增加赌注
     */
    public setItemW(player: ZJHPlayer, allBet: number): void {
        this.itemW.setCards(false, player.cards);
        this.itemW.nameLabel.string = player.name;
        this.itemW.scoreLabel.string = player.state ? "赢": "输";
        this.itemW.betLabel.string = player.state ? "+"+allBet: "-"+player.sum;
    }

}
