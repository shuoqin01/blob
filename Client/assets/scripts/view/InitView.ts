/*
 * @Author: fasthro
 * @Description: 启动界面
 * @Date: 2019-03-28 18:47:22
 */

const { ccclass, property, menu } = cc._decorator;

@ccclass
@menu("View/InitView")
export default class InitView extends cc.Component {
    // 进度条
    @property(cc.ProgressBar)
    public progressBar: cc.ProgressBar = null;

    // 进度文本
    @property(cc.Label)
    public rateLable: cc.Label = null;

    private active: boolean = false;

    onLoad() {
        this.active = false;
        this.progressBar = this.node.getChildByName("progress").getComponent(cc.ProgressBar);
        this.rateLable = this.progressBar.node.getChildByName("rate").getComponent(cc.Label);
        cc.log("InitView onLoad==> rate=" + this.rateLable.string);
    }
    
    start(){
        this.progressBar.progress = 0;
        this.rateLable.string = "0%";
        this.active = true;
    }

    update(){
        if(!this.active)
            return;
        if(this.progressBar.progress >= 1){
            this.setProgressBar(1);
            return;
        }
        this.setProgressBar(this.progressBar.progress+0.01);
        // cc.log("InitView update==> progress = "+this.progressBar.progress);
    }

    private setProgressBar( progress: number){
        this.progressBar.progress = progress;
        this.rateLable.string = (progress*100).toFixed(1) +"%";
    }
}
