import GameInfos, { GameType, GameInfo } from "../define/Games";
import BaseGame from "../game/center/BaseGame";

/*
 * @Author: fasthro
 * @Description: 游戏中心服务
 * @Date: 2019-04-03 15:12:01
 */

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameCenter {
    // 初始化标志
    private static initialized: boolean = false;

    //游戏列表
    private static games: {[key:string]: BaseGame};

    /**
     * 初始化游戏中心服务
     */
    public static initialize(): void {
        this.games = {};

        // 初始化游戏信息
        GameInfos.initialize();

         // 添加并初始化控制器
        //  for(let key in GameInfos.infos){
        //     let info = GameInfos.infos[key];
        //     let game = info.game.create(info.name, info.controllerType);
        //     game.initialize();
        //     this.games[key] = game;
        //  }

        this.initialized = true;
    }

    /**
     * 获取游戏信息
     * @param t 
     * 
     * deprecated
     */
    public static getGameInfo(t: GameType): GameInfo {
        return GameInfos.getGameInfo(t);
    }

    /**
     * 获取哪类游戏
     */
    public static get<T extends BaseGame>(t:GameType): T{
        return <T> this.games[t];
    }

     /**
      * update
      */
     public static update(dt): void{
        //  if(this.initialized){
        //      for(let key in this.games){
        //          let game = this.games[key];
        //          if(game != null){
        //              game.update(dt);
        //          }
        //      }
        //  }
     }
}
