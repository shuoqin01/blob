module ZJH {
    /**
     * 牌型和基础： 
     * - 牌的对应关系
     * - 大王 小王 黑桃A 红桃A 草花A 方片A  黑桃2 红桃2 草花2 方片2 黑桃3 红桃3 草花3 方片3 ...
     * -  2    3     4    5    6     7     8     9    10    11    12   13    14    15  ...
     * 
     *  大 - 2
     *  小 - 3
     *  2  - 8,9,10,11
     *  3  - 12,13,14,15
     *  4  - 16,17,18,19
     *  5  - 20,21,22,23
     *  6  - 24,25,26,27
     *  7  - 28,29,30,31
     *  8  - 32,33,34,35
     *  9  - 36,37,38,39
     *  10 - 40,41,42,43
     *  J  - 44,45,46,47
     *  Q  - 48,49,50,51
     *  K  - 52,53,54,55
     *  A  - 4,5,6,7
     */
    export class Core {

        // 所有的牌数组
        private static _cards: Array<number> = [];
        // 所有的牌数组：去掉  大 小
        public static get cards(): any {
            if (this._cards.length == 0) {
                for (let i = 4; i <= 55; i++) {
                    this._cards.push(i);
                }
            }
            return this._cards;
        }

        /**
         * 洗牌：洗1-8遍；随机找一张牌和每张牌进行交换
         */
        public static disorder(): void {
            let tryCount = Utils.random(1, 8);
            let cardCount = this.cards.length;
            for (let i = 0; i < tryCount; i++) {
                for (let k = 0; k < this.cards.length; k++) {
                    let r = Utils.random(k, cardCount - 1);
                    let temp = this.cards[k];
                    this.cards[k] = this.cards[r];
                    this.cards[r] = temp;
                }
            }
        }

        /**
         * 发牌： 每人三张牌, 然后从牌大到小排序
         * @returns xyzwv: 玩家牌
         */
        public static deal(): any {
            this.disorder();//洗牌

            // let x: Array<number> = [8,50, 47];
            // let y: Array<number> = [20,24,28];
            // let z: Array<number> = [9, 13, 21];
            // let w: Array<number> = [30, 39,32];
            // let v: Array<number> = [4,5,6];

            let x: Array<number> = [];
            let y: Array<number> = [];
            let z: Array<number> = [];
            let w: Array<number> = [];
            let v: Array<number> = [];

            for (let i = 0; i < 5*3; i++) {
                switch(i % 5){
                    case 0:
                        x.push(this.cards[i]);
                        break;
                    case 1:
                        y.push(this.cards[i]);
                        break;
                    case 2:
                        z.push(this.cards[i]);
                        break;
                    case 3:
                        w.push(this.cards[i]);
                        break;
                    case 4:
                        v.push(this.cards[i]);
                        break;
                }
            }
            x = this.sortNormal(x);
            y = this.sortNormal(y);
            z = this.sortNormal(z);
            w = this.sortNormal(w);
            v = this.sortNormal(v);
            cc.log(`发牌：x= ${x}, y= ${y}, z= ${z}, w= ${w},v = ${v}`);
            return { w: w, x: x, y: y, z: z , v: v};
        }

        /**
         * 排序-普通: 按正常大小排序：即A K Q ... 2
         * @param cards 
         * @returns 返回排序后的牌列表
         */
        public static sortNormal(cards: Array<number>): Array<number> {
            //修改牌的数据： 这里只修改A的大小，4 5 6 7变成56 57 58 59
            let absCards = this.absoluteCards(cards);
            //sort是排序字符串的，一位数和两位数排序会出现问题，比如4和32，4会比32大；所以需要自己实现一个方法
            absCards = this._sortNum(absCards);
            absCards.reverse();//从大到小排列
            return this.unAbsoluteCards(absCards);//修改的数据还原
        }

        //排序数值: 从小到大排列
        private static _sortNum(absCards: Array<number>): Array<number> {
            let temp: number;
            for(let i=0; i< absCards.length-1; i++){
                if(absCards[i] > absCards[i+1]) {
                    temp = absCards[i];
                    absCards[i] = absCards[i+1];
                    absCards[i+1] = temp;
                }
            }
            if(absCards[0] > absCards[1]) {
                temp = absCards[0];
                absCards[0] = absCards[1];
                absCards[1] = temp;
            }
            return absCards;
        }


        /**
         * 是不是豹子: 三张牌一样
         * @param cards 
         */
        public static isLeopard(cards: Array<number>): boolean {
            let c1 = Math.floor(cards[0] / 4);
            let c2 = Math.floor(cards[1] / 4);
            let c3 = Math.floor(cards[2] / 4);
            return c1 == c2 && c2 == c3;
        }

        /**
         * 是不是顺金： 同花顺
         * @param cards 
         */
        public static isStraightSameColor(cards: Array<number>): boolean {
            return this.isSameColor(cards) && this.isStraight(cards);
        }

        /**
         * 是不是金花： 同花，　包括同花顺
         * @param cards 
         */
        public static isSameColor(cards: Array<number>): boolean {
            let c1 = cards[0] % 4;
            let c2 = cards[1] % 4;
            let c3 = cards[2] % 4;

            return c1 == c2 && c2 == c3;
        }

        /**
         * 是不是顺子： 包括同花顺； 不考虑 A23 ； AKQ是顺子
         * @param cards 
         */
        public static isStraight(cards: Array<number>): boolean {
            if(cards[1] < 8) return false;
            let card0 = cards[0];
            //考虑A的情况 4 5 6 7
            if(card0 < 8) card0 = this.absoluteCard(card0);

            let c1 = Math.floor(cards[0] / 4);
            let c2 = Math.floor(cards[1] / 4);
            let c3 = Math.floor(cards[2] / 4);

            return c1 - c2 == 1 && c2 - c3 == 1;
        }


        /**
         * 是不是对子：包括豹子
         * @param cards 
         */
        public static isPair(cards: Array<number>): boolean {
            let c1 = Math.floor(cards[0] / 4);
            let c2 = Math.floor(cards[1] / 4);
            let c3 = Math.floor(cards[2] / 4);
            return c1 == c2 || c2 == c3;
        }

        /**
         * 是不是特殊牌：2 3 5； 普通散牌不用考虑
         * @param cards 
         */
        public static isSpecial(cards: Array<number>): boolean {
            let c1 = Math.floor(cards[0] / 4);//5
            let c2 = Math.floor(cards[1] / 4);//3
            let c3 = Math.floor(cards[2] / 4);//2
            return c1 == 5 && c2 == 3 && c3 == 2;
        }

        /**
         * 比较牌大小： 思路， 从大到小比，都是分两种情况比
         * @param ocards 自己的牌
         * @param vcards 对手的牌
         * @returns 0 ocards == vcards, 1 ocards > vcards, -1 ocards < vcards
         */
        public static compareCards(ocards: Array<number>, vcards: Array<number>): number {
            //豹子: 1 都是豹子 ； 2 一个是豹子
            if(this.isLeopard(ocards) && this.isLeopard(vcards)){
                return this._compareSingleCard(ocards[0], vcards[0]);
            }
            if(this.isLeopard(ocards)){
                return this.isSpecial(vcards) ? -1: 1;
            }
            if(this.isLeopard(vcards)){
                return this.isSpecial(ocards) ? 1: -1;
            }

            //顺金
            if(this.isStraightSameColor(ocards) && this.isStraightSameColor(vcards)){
                return this._compareSingleCard(ocards[0], vcards[0]);
            }
            if(this.isStraightSameColor(ocards)) return 1;
            if(this.isStraightSameColor(vcards)) return -1;

            //金花
            if(this.isSameColor(ocards) && this.isSameColor(vcards)){
                return this._compareSingleCard(ocards[0], vcards[0]);
            }
            if(this.isSameColor(ocards)) return 1;
            if(this.isSameColor(vcards)) return -1;

            //顺子
            if(this.isStraight(ocards) && this.isStraight(vcards)){
                return this._compareSingleCard(ocards[0], vcards[0]);
            }
            if(this.isStraight(ocards)) return 1;
            if(this.isStraight(vcards)) return -1;

            //对子
            if(this.isPair(ocards) && this.isPair(vcards)){
                return this._compareSingleCard(ocards[1], vcards[1]);
            }
            if(this.isPair(ocards)) return 1;
            if(this.isPair(vcards)) return -1;

            //散牌
            return this._compareSingleCard(ocards[0], vcards[0]);
        }

        /**
         * 比较单牌大小
         * @param ocard 自己的牌
         * @param vcard 对手的牌
         * @returns 0 ocard == vcard, 1 ocard > vcard, -1 ocard < vcard
         */
        private static _compareSingleCard(ocard: number, vcard: number): number {
            let oc = Math.floor(this.absoluteCard(ocard) / 4);
            let vc = Math.floor(this.absoluteCard(vcard) / 4);
            if (oc > vc) return 1;
            else if (oc < vc) return -1;
            else return 0;
        }

        /**
         * 将外部数据的牌编号,变成内部方便比较大小的绝对值
         * 只修改 4 5 6 7  ==> 56 57 58 59
         * @param card 
         * @returns card 绝对值
         */
        private static absoluteCard(card: number): number {
            if (card > 7) return card;
            return card + 52;
        }

        /**
         * 将外部数据的牌编号,变成内部方便比较大小的绝对值
         * @param cards 
         * @returns cards 绝对值
         */
        private static absoluteCards(cards: Array<number>): Array<number> {
            let ncs: Array<number> = [];
            for (let i = 0; i < cards.length; i++) {
                ncs.push(this.absoluteCard(cards[i]));
            }
            return ncs;
        }

        /**
         * 将内部数据的绝对值,变成外部牌编号
         * @param card 
         * @returns card 牌编号
         */
        private static unAbsoluteCard(card: number): number {
            if (card < 56) return card;
            return card - 52;
        }

        /**
         * 将内部数据的绝对值,变成外部牌编号
         * @param cards 
         * @returns cards 牌编号
         */
        private static unAbsoluteCards(cards: Array<number>): Array<number> {
            let ncs: Array<number> = [];
            for (let i = 0; i < cards.length; i++) {
                ncs.push(this.unAbsoluteCard(cards[i]));
            }
            return ncs;
        }

        /**
         * 获取牌型
         * @param cards 
         */
        public static getCardsType(cards: Array<number>): string{
            let pattern = new Pattern();
            pattern.cards = cards;
            if(this.isLeopard(cards)) pattern.type = PatternType.LEOPARD;
            else if( this.isStraightSameColor(cards)) pattern.type = PatternType.STRAIGHTCOLOR;
            else if( this.isSameColor(cards)) pattern.type = PatternType.COLOR;
            else if( this.isStraight(cards)) pattern.type = PatternType.STRAIGHT;
            else if( this.isPair(cards)) pattern.type = PatternType.PAIR;
            else if( this.isSpecial(cards)) pattern.type = PatternType.SPECIAL;
            else pattern.type = PatternType.NORMAL;

            return pattern.tostring();
        }

    }

    /**
     * 牌型类型
     */
    const PatternType = cc.Enum({
        LEOPARD: 0,
        STRAIGHTCOLOR: 1,
        COLOR: 2,
        STRAIGHT: 3,
        PAIR: 4,
        NORMAL: 5,
        SPECIAL: 6,
    });

    /**
     * 牌型
     */
    export class Pattern {
        // 牌
        public cards: Array<number>;
        // 牌型类型
        public type: number;

        public tostring(): string {
            // 牌型
            let cpt = "";
            if (this.type == PatternType.LEOPARD) cpt = "豹子"
            else if (this.type == PatternType.STRAIGHTCOLOR) cpt = "顺金"
            else if (this.type == PatternType.COLOR) cpt = "金花"
            else if (this.type == PatternType.STRAIGHT) cpt = "顺子"
            else if (this.type == PatternType.PAIR) cpt = "对子"
            else if (this.type == PatternType.NORMAL) cpt = "散牌"
            else if (this.type == PatternType.NORMAL) cpt = "特殊牌"

            // 牌
            let cs = "";
            for (let i = 0; i < this.cards.length; i++) {
                let card = this.cards[i];
                let str = "";
                if (card >= 4 && card <= 7) str = "A"
                else {
                    let num = Math.floor(card / 4);
                    if (num == 11) str = "J"
                    else if (num == 12) str = "Q"
                    else if (num == 13) str = "K"
                    else str = num.toString();
                }

                cs += str;
                if (i < this.cards.length - 1) cs += ",";
            }
            return `牌型: ${cpt} - 牌: ${cs}`;
        }
    }

    /**
     * AI
     */
    export class AI {


        /**
         * 判断对象是否存在
         * @param obj 
         */
        private static _existObj(obj: any): boolean {
            return obj != null && obj != undefined;
        }

        
        public static test(cards: Array<number>) {
        }
    }

    /**
     * 工具
     */
    export class Utils {
        /**
         * 随机数
         * @param minValue 
         * @param maxValue 
         */
        public static random(minValue: number, maxValue: number): number {
            return Math.round(Math.random() * (maxValue - minValue) + minValue);
        }
    }
}

export = ZJH;
