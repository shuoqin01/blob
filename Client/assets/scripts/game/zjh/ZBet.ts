// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property, menu } = cc._decorator;

@ccclass
@menu("Game/ZBet")
export default class ZBet extends cc.Component {

    private _bet: cc.Sprite = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this._initComponent();
    }

    private _initComponent(){
        if(!this._bet){
            this._bet = this.node.getChildByName("zbet").getComponent(cc.Sprite);
        }
    }
    start () {
 
    }

    public addBet(betIcon: cc.SpriteFrame,betSize: cc.Vec2){
        this._initComponent();
        this._bet.spriteFrame = betIcon;
        this._bet.node.setContentSize(betSize.x, betSize.y);
    }

    // update (dt) {}
}
