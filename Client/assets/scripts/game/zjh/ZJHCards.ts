import Card from "./ZJHCard";

/*
 * @Author: fasthro
 * @Description: 牌组件
 * @Date: 2019-04-02 11:19:07
 */

const { ccclass, property, menu } = cc._decorator;

@ccclass
@menu("Game/ZJHCards")
export default class ZJHCards extends cc.Component {
    // 图集
    @property(cc.SpriteAtlas)
    public atlas: cc.SpriteAtlas = null;

    // card perfab
    @property(cc.Prefab)
    public cardPrefab: cc.Prefab = null;

    // 牌的尺寸
    @property(cc.Vec2)
    public cardSize: cc.Vec2 = new cc.Vec2(77, 100);

    // 牌列表
    private _cards: Array<Card> = [];

    /**
     * 初始化牌: 添加牌
     * @param cards 牌数组
     */
    public initCards(cIds: number[], isResult:boolean = false): void {
        let count = cIds.length;
        for (let i = 0; i < count; i++) {
            this._cards.push(this._createCard(cIds[i], i, count, isResult));
        }
    }

    /**
     * 创建牌
     * @param cId 牌编号
     * @param index 牌角标
     * @param total 总牌数
     */
    private _createCard(cId: number, index: number, total: number, isResult:boolean = false): Card {
        let cardNode: cc.Node = cc.instantiate(this.cardPrefab);
        this.node.addChild(cardNode);
        let card: Card = cardNode.getComponent(Card);
        this._resetCard(card, cId, index, total, isResult);
        return card;
    }

    /**
     * 重置card
     * @param card 
     * @param cId 
     * @param index 
     * @param total 
     */
    private _resetCard(card: Card, cId: number, index: number, total: number, isResult:boolean = false): void {
        card.node.name = cId.toString();
        card.node.setPosition(this._getCardPosition(index, total, isResult));
        card.node.setSiblingIndex(index);

        // init
        card.initCard(cId, this.cardSize, this.atlas, null);
    }


    /**
     * 获取牌的坐标
     * @param index 牌的索引
     * @param total 总牌数
     */
    private _getCardPosition(index: number, total: number, isResult:boolean = false): cc.Vec2 {
        cc.log("cardSize="+this.cardSize.x)
        let x = isResult? -120+index*this.cardSize.x/2: -120 +　index * this.cardSize.x;

        let y = -44;
        cc.log(index + " 牌坐标为 x=" + x + ",y=" + y);
        return new cc.Vec2(x, y);
    }

}

