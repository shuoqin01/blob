import ZJH = require("./ZJH");

/**
 * 事件回调数据类型
 */
export class ZJHEventData {
    public player?: ZJHPlayer;          // 事件执行玩家
    public endstate?: number;            // 游戏中止状态
    public cards?: Array<number>;       // 手中牌
    public pickId?: number;             //选择比牌的id

    public timeout?: number;            // 倒计时
    public force?: boolean;             // 强制出牌

    public playerX:ZJHPlayer;
    public playerY:ZJHPlayer;
    public playerZ:ZJHPlayer;
    public playerW:ZJHPlayer;
    public playerV:ZJHPlayer;
    public sum: number;

    tostring(): string {
        return `
        player:${this.player.name} 
        cards:${this.cards ? this.cards : "null"} 
        timeout:${this.timeout != null || this.timeout != undefined ? this.timeout : "null"}
        endstate:${this.endstate != null || this.endstate != undefined ? this.endstate : "null"}
        pickId:${this.pickId != null || this.pickId != undefined ? this.pickId : "null"}
        force:${this.force ? this.force : "null"}`
    }
}

/**
 * 事件回调类型
 */
export type ZJHEvent = {
    name?: string;
    handler(data: ZJHEventData): void;
    context: any;
}

/**
 * 游戏中止状态: 弃牌； 比牌失败
 */
export enum ZJHEndState {
    drop,
    fail,
}

//定义常量的一种方式
interface ConfigInterface {
    //强制选择时间
    readonly CHOICE_TIME: number;
    //托管出牌时间
    readonly AGENT_TIME: number;
    //结果页面显示时间
    readonly RESULT_SHOW_TIME: number;
    //最大赌注
    readonly MAX_BET_COUNT: number;
    //赌注array
    readonly BET_ARRAY: Array<number>;
}

export const Config: ConfigInterface = {
    //强制选择时间
    CHOICE_TIME:  30,

    AGENT_TIME:   5000,
    RESULT_SHOW_TIME: 10000,

    //最大赌注
    MAX_BET_COUNT: 1000,

    //赌注array
    BET_ARRAY: [100, 500, 1000],
}

/**
 * 炸金花 - 玩家数据
 */
export class ZJHPlayer {

    //状态：是否还在玩；
    private _state: boolean = false;
    public get state(): boolean { return this._state; }
    public set state(value) { this._state = value; }

    // round
    private _round: ZJHRound = null;

    // 玩家id
    private readonly _id: number = 0;
    public get id(): number { return this._id; }

    // 玩家名称
    private readonly _name: string = "";
    public get name(): string { return this._name; }

    // 玩家头像
    private readonly _head: string = "";
    public get head(): string { return this._head; }

    // 玩家代币: 玩家拥有的代币
    private _coin: number = 0;
    public get coin(): number { return this._coin; }
    public set coin(value) { this._coin = value; }

    // 是否为庄家
    private _isLord: boolean = false;
    public get isLord(): boolean { return this._isLord; }
    public setLord(lord: boolean) {
        this._isLord = lord;
    }

    //托管
    private _agent: boolean = false;
    public get agent():boolean { return this._agent;}
    public set agent(value) { this._agent = value; }

    // 手里牌
    private _cards: Array<number> = [];
    public get cards(): Array<number> { return this._cards; }
    public set cards(value) { this._cards = value; }

    // 是否为操作方，点击准备
    private _owner: boolean = false;
    public get owner(): boolean { return this._owner; }
    public set owner(value) { this._owner = value; }

    //下一个玩家
    public nextPlayer:ZJHPlayer = null;

    // 连胜次数
    private _winCount: number = 0;
    public get winCount(): number { return this._winCount; }

    // 倒计时
    private _timeout: number = 0;
    // 倒计时handler
    private _timeoutHandler: number = null;

    // 强制出牌标志
    private _isForce: boolean = false;

    // 准备
    public readyEvent: ZJHEvent = null;
    // 发牌
    public dealEvent: ZJHEvent = null;

    // 跟注
    public followEvent: ZJHEvent = null;
    // 加注
    public addEvent: ZJHEvent = null;
    //比牌
    public vsEvent: ZJHEvent = null;
    //选择比牌
    // public pickvsEvent: ZJHEvent = null;
    // 看牌
    public flipEvent: ZJHEvent = null;
    // 弃牌
    public dropEvent: ZJHEvent = null;

    //玩家赌注：看牌赌注变为高一档，如100-->500 单注
    private _score: number = 0;
    public get score() { return this._score; }

    //总注
    private _sum: number = 0;
    public get sum(){ return this._sum;}

    //赌注角标: 看牌的时候用，因为赌注并不一定是翻倍的
    private _betIndex: number = 0
    public get betIndex() { return this._betIndex; }

    //是否看牌：用来计算玩家赌注
    private _isFlip: boolean = false;
    public get isFlip() { return this._isFlip; }

    // 产生
    public createLordEvent: ZJHEvent = null;
    // 操作计时
    public timeoutEvent: ZJHEvent = null;

    /**
     * 构造方法
     * @param name 名称
     * @param head 头像
     */
    constructor(round: ZJHRound, id: number, name: string, head: string, coin: number, score: number) {
        this._round = round;
        this._id = id;
        this._name = name;
        this._head = head;
        this._coin = coin;
        this._winCount = 0;
        this._score = score;
        this._betIndex = 0;
        this._isFlip = false;
        this._sum = score;
    }

    /**
     * 初始化
     * @param owner 是否为操作者
     * @param agent 是否托管
     * @param state 是否还在游戏中
     */
    public init(owner: boolean = false, agent: boolean = false): void {
        this._owner = owner;
        this._isLord = false;
        this._cards = [];
        this._isForce = false;
        this._state = true;
        this._betIndex = 0;
        this._isFlip = false;
        this._score = Config.BET_ARRAY[0];
        this._sum = this._score;
        this._agent = agent;
    }

    /**
     * 倒计时
     */
    private _setTimeout(timeout: number, state?: number): void {
        this._timeout = timeout;

        let data: ZJHEventData = new ZJHEventData();
        data.timeout = this._timeout;
        this._callEvent(this.timeoutEvent, data);

        let self = this;
        this.clearTimeout();
        this._timeoutHandler = setInterval(() => {
            self._timeout--;
            if (self._timeout <= 0) {
                //直接过牌
                cc.log(`timeout = ${self._timeout}, playerid=${this._id}`);
                this.follow();
                // this.clearTimeout();
            }
            else {
                let data: ZJHEventData = new ZJHEventData();
                data.timeout = self._timeout;
                self._callEvent(this.timeoutEvent, data);
            }
        }, 1000);
    }

    /**
     * 清空倒计时
     */
    public clearTimeout(): void {
        if (this._timeoutHandler) clearTimeout(this._timeoutHandler);
    }

    /**
     * 准备
     */
    public ready(): void {
        this._coin -= this._score;
        if (this.owner) {

            this._callEvent(this.readyEvent);
        }
    }

    /**
     * 发牌
     * @param cards 
     */
    public deal(cards: Array<number>): void {
        this._cards = cards;
        if (this.owner) {
            // let data: ZJHEventData = new ZJHEventData();
            // data.cards = cards;
            this._callEvent(this.dealEvent);
        }
    }

    //设置倒计时
    public choiceTimeout() {
        this._setTimeout(Config.CHOICE_TIME);
    }

    //跟注
    public follow() {
        this.clearTimeout();
        if(this.isFlip && this._round.score < Config.MAX_BET_COUNT) {
            this._betIndex = this._round.betIndex + 1;
            this._score = Config.BET_ARRAY[this._betIndex];
        }else{
            this._betIndex = this._round.betIndex;
            this._score = this._round.score;
        }
        this._updateCoinAndBet(this._score);
        this._callEvent(this.followEvent);
    }

    //加注: 先计算赌注，然后计算消耗多少代币
    public add(index: number) {
        this.clearTimeout();
        if(index >= Config.BET_ARRAY.length) return;
        this._round.score = Config.BET_ARRAY[index];
        this._round.betIndex = index;
        if (this._isFlip && index < Config.BET_ARRAY.length -  1) {
            this._betIndex = index + 1;
            this._score = Config.BET_ARRAY[index + 1];
        } else {
            this._betIndex = index;
            this._score = Config.BET_ARRAY[index];
        }
        this._updateCoinAndBet(this._score);
        this._callEvent(this.addEvent);
    }

    //获取赌注角标
    public getBetIndex(): number{
        if(this._round.score == Config.MAX_BET_COUNT){
            return Config.BET_ARRAY.length - 1;
        }
        if(this._isFlip) return this._betIndex -1;
        return this._betIndex;
    }

    //看牌
    public flip() {
        this._isFlip = true;
        if (this._round.score < Config.MAX_BET_COUNT) {
            this._betIndex = this._round.betIndex + 1;
            this._score = Config.BET_ARRAY[this._betIndex];
        }else{
            this._betIndex = this._round.betIndex;
            this._score = this._round.score;
        }
        this._callEvent(this.flipEvent);
    }

    //比牌
    public vs() {

    }

    //选择比牌
    public pickVs(pickId: number){
        this.clearTimeout();
        //规则：需要支付单注两倍的费用
        this._updateCoinAndBet(this._round.score * 2);
        let data = new ZJHEventData();
        data.pickId = pickId;
        this._callEvent(this.vsEvent, data);
    }

    //更新玩家coin和赌注
    private _updateCoinAndBet( count: number){
        this._sum += count;
        this.coin -= count;
    }

    //弃牌
    public drop() {
        this.clearTimeout();
        this._state = false;
        this._round.playerNum -= 1;
        this._callEvent(this.dropEvent);
    }

    /**
     * 产生庄
     * @param cards 底牌
     */
    public createLord(): void {
        this._isLord = true;
        this._callEvent(this.createLordEvent);
    }

    /**
     * 执行事件
     * @param event 
     * @param data 
     */
    private _callEvent(event: ZJHEvent, data?: ZJHEventData): void {
        if (event) {
            if (!data) {
                data = new ZJHEventData();
            }
            data.player = this;
            data.cards = this._cards;
            if (event.name != "_onTimeoutEvent") {
                console.log(`${this.name} event: ${event.name} data:${data.tostring()}`);
            }
            event.handler.call(event.context, data);
        }
    }
}

/**
 * 炸金花 - 本轮数据
 */
export class ZJHRound {

    // 玩家X - 左边1玩家
    public playerX: ZJHPlayer = null;

    // 玩家Y - 右边1玩家
    public playerY: ZJHPlayer = null;

    // 玩家Z - 左边2玩家
    public playerZ: ZJHPlayer = null;

    // 玩家w - 右边2玩家
    public playerW: ZJHPlayer = null;

    // 玩家v - 操作方
    public playerV: ZJHPlayer = null;

    // 赌注: 基本赌注， 单注
    public score: number = 0;
    //赌注：角标索引
    public betIndex: number = 0;
    //赌注： 总注
    public sum: number = 0;

    // 轮数: 暂时没有用
    public loop: number = 0;

    // 玩家人数
    public playerNum: number = 0;


    // 本轮开始的玩家
    private _first: ZJHPlayer = null;
    public get first(): ZJHPlayer{return this._first;}
    // 本轮当前进行的玩家
    private _current: ZJHPlayer = null;
    public get current(): ZJHPlayer{return this._current;}

    // 事件
    // 重新开始
    private _breakEvent: ZJHEvent = null;
    // over
    private _overEvent: ZJHEvent = null;

    /**
     * 比赛流局重新开始
     * @param score 底分
     * @param breakEvent 比赛中断事件
     * @param overEvent 比赛结束事件
     */
    constructor(score: number, breakEvent: ZJHEvent, overEvent: ZJHEvent) {
        this.score = score;
        this._breakEvent = breakEvent;
        this._overEvent = overEvent;

        this.playerX = new ZJHPlayer(this, 1, "x玩家", "mm", 100000, score);
        this.playerY = new ZJHPlayer(this, 2, "y玩家", "sys_mm", 200000, score);
        this.playerZ = new ZJHPlayer(this, 3, "z玩家", "gg", 300000, score);
        this.playerW = new ZJHPlayer(this, 4, "w玩家", "wanren-mm", 400000, score);
        this.playerV = new ZJHPlayer(this, 5, "v玩家", "owner", 500000, score);

        this.playerX.nextPlayer = this.playerZ;
        this.playerY.nextPlayer = this.playerX;
        this.playerZ.nextPlayer = this.playerV;
        this.playerW.nextPlayer = this.playerY;
        this.playerV.nextPlayer = this.playerW;
        this.playerNum = 5;
        this.sum = this.playerNum * this.score;
    }

    /**
     * 初始化
     */
    public init(): void {
        this.loop = 1;
        this._first = null;
        this._current = null;
        this.score = Config.BET_ARRAY[0];
        this.playerNum = 5;
        this.sum = this.playerNum * this.score;
        this.betIndex = 0;

        this.playerX.init(false, true);
        this.playerY.init(false, true);
        this.playerZ.init(false, true);
        this.playerW.init(false, true);
        this.playerV.init(true, false);
    }

    /**
     * 绑定倒计时事件
     * @param event 
     */
    public bindTimeoutEvent(event: ZJHEvent): void {
        this.playerX.timeoutEvent = event;
        this.playerY.timeoutEvent = event;
        this.playerZ.timeoutEvent = event;
        this.playerW.timeoutEvent = event;
        this.playerV.timeoutEvent = event;
    }


    /**
     * 绑定准备事件
     * @param event 
     */
    public bindReadyEvent(event: ZJHEvent): void {
        this.playerX.readyEvent = event;
        this.playerY.readyEvent = event;
        this.playerZ.readyEvent = event;
        this.playerW.readyEvent = event;
        this.playerV.readyEvent = event;
    }

    /**
     * 准备
     * @param event 
     */
    public ready(): void {
        this.playerX.ready();
        this.playerY.ready();
        this.playerZ.ready();
        this.playerW.ready();
        this.playerV.ready();
    }

    /**
     * 绑定发牌事件
     * @param event 
     */
    public bindDealEvent(event: ZJHEvent): void {
        this.playerX.dealEvent = event;
        this.playerY.dealEvent = event;
        this.playerZ.dealEvent = event;
        this.playerW.dealEvent = event;
        this.playerV.dealEvent = event;
    }

    /**
     * 绑定跟注事件
     * @param event 
     */
    public bindFollowEvent(event: ZJHEvent): void {
        this.playerX.followEvent = event;
        this.playerY.followEvent = event;
        this.playerZ.followEvent = event;
        this.playerW.followEvent = event;
        this.playerV.followEvent = event;
    }

    /**
     * 绑定加注事件
     * @param event 
     */
    public bindAddEvent(event: ZJHEvent): void {
        this.playerX.addEvent = event;
        this.playerY.addEvent = event;
        this.playerZ.addEvent = event;
        this.playerW.addEvent = event;
        this.playerV.addEvent = event;
    }

    /**
     * 绑定看牌事件
     * @param event 
     */
    public bindFlipEvent(event: ZJHEvent): void {
        this.playerX.flipEvent = event;
        this.playerY.flipEvent = event;
        this.playerZ.flipEvent = event;
        this.playerW.flipEvent = event;
        this.playerV.flipEvent = event;
    }

    /**
     * 绑定比牌事件
     * @param event 
     */
    public bindVsEvent(event: ZJHEvent): void {
        this.playerX.vsEvent = event;
        this.playerY.vsEvent = event;
        this.playerZ.vsEvent = event;
        this.playerW.vsEvent = event;
        this.playerV.vsEvent = event;
    }

    /**
     * 绑定弃牌事件
     * @param event 
     */
    public bindDropEvent(event: ZJHEvent): void {
        this.playerX.dropEvent = event;
        this.playerY.dropEvent = event;
        this.playerZ.dropEvent = event;
        this.playerW.dropEvent = event;
        this.playerV.dropEvent = event;
    }

    /**
     * 发牌
     */
    public deal(): void {
        //获取牌
        let cards = ZJH.Core.deal();
        // 玩家发牌
        this.playerX.deal(cards.x);
        this.playerY.deal(cards.y);
        this.playerZ.deal(cards.z);
        this.playerW.deal(cards.w);
        this.playerV.deal(cards.v);

    }

    //少于两个玩家时游戏结束
    public isGameOver(): boolean {
        return this.playerNum < 2;
    }

    /**
     * 执行中止事件
     * 
     */
    public executeBreakEvent(player: ZJHPlayer, endState: number): void {
        let data = new ZJHEventData();
        data.endstate = endState;
        data.player = player;

        this._callEvent(this._breakEvent, data);
    }

    public findNextPlayer():ZJHPlayer{
        //根据当前玩家，找到邻桌，判断状态和比对第一个玩家，设置轮数，找到在线玩家
        let player = this._current.nextPlayer;
        while(player.id != this._current.id){
            if(player.id == this._first.id) this.loop++;
            if(player.state){
                this._current = player;
                return player;
            }
            player = player.nextPlayer;
        }
        throw new Error("findNextPlayer ==> error");
    }

    /**
     * 绑定产生事件
     * @param event 
     */
    public bindCreateLordEvent(event: ZJHEvent): void {
        this.playerX.createLordEvent = event;
        this.playerY.createLordEvent = event;
        this.playerZ.createLordEvent = event;
        this.playerW.createLordEvent = event;
        this.playerV.createLordEvent = event;
    }

    /**
     * 产生随机庄
     */
    public createRandomLord(): void {
        this.createLord(this._randomPlayer());
    }
    /**
     * 产生庄
     */
    public createLord(player: ZJHPlayer): void {
        this._first = player;
        this._current = player;

        // 记录倍数
        this.loop = 1;
        player.createLord();

    }

    //结束
    public overRound(data:ZJHEventData) {
        //处理结果
        this._setResult(data.player.id);
        this._callEvent(this._overEvent,data);
    }

    private _setResult(playerId: number) {
        switch(playerId){
            case this.playerX.id:
                this.playerX.coin += this.sum;
                break;
            case this.playerY.id:
                this.playerY.coin += this.sum;
                break;
            case this.playerZ.id:
                this.playerZ.coin += this.sum;
                break;
            case this.playerW.id:
                this.playerW.coin += this.sum;
                break;
            case this.playerV.id:
                this.playerV.coin += this.sum;
                break;
        }
    }
    /**
     * 执行事件
     * @param event 
     * @param data 
     */
    private _callEvent(event: ZJHEvent, data?: ZJHEventData): void {
        if (event) {
            console.log(`Round event: ${event.name} data:${data ? data.tostring() : "null"}`);
            event.handler.call(event.context, data);
        }
    }

    /**
     * 是否为X玩家
     * @param player 
     */
    public isPlayerX(playerId: number): boolean {
        return this.playerX && playerId == this.playerX.id;
    }

    /**
     * 是否为Y玩家
     * @param player 
     */
    public isPlayerY(playerId: number): boolean {
        return this.playerY && playerId == this.playerY.id;
    }

    /**
     * 是否为Z玩家
     * @param player 
     */
    public isPlayerZ(playerId: number): boolean {
        return this.playerZ && playerId == this.playerZ.id;
    }

    /**
     * 是否为w玩家
     * @param player 
     */
    public isPlayerW(playerId: number): boolean {
        return this.playerW && playerId == this.playerW.id;
    }

    /**
     * 是否为v玩家
     * @param player 
     */
    public isPlayerV(playerId: number): boolean {
        return this.playerV && playerId == this.playerV.id;
    }

    /**
     * 随机玩家
     * @returns 返回随机的玩家
     */
    private _randomPlayer(): ZJHPlayer {
        let tryCount = 8;
        let players: Array<ZJHPlayer> = [this.playerX, this.playerY, this.playerZ, this.playerW, this.playerV];
        for(let j= 0; j< tryCount; j++)
            for (let i = 0; i < players.length; i++) {
                let r = ZJH.Utils.random(i, players.length - 1);
                let temp = players[i];
                players[i] = players[r];
                players[r] = temp;
            }
        return players[0];
    }

    /**
     * 随机比牌玩家
     * @returns 返回随机的玩家id
     */
    public randomVsPlayer(ownerId: number): number {
        let tryCount = 8;
        let players: Array<ZJHPlayer> = [this.playerX, this.playerY, this.playerZ, this.playerW, this.playerV];
        for(let j= 0; j< tryCount; j++)
            for (let i = 0; i < players.length; i++) {
                let r = ZJH.Utils.random(i, players.length - 1);
                let temp = players[i];
                players[i] = players[r];
                players[r] = temp;
            }
        for(let i=0; i<players.length; i++){
            let player = players[i];
            if(player.state && ownerId != player.id){
                return player.id;
            }
        }
        throw new Error("找不到随机比牌玩家");
    }

}
