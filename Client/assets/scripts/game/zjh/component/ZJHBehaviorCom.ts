import ZJHCards from "../ZJHCards";
import { ZJHEndState } from "../ZJHRound";

/*
 * @Author: fasthro
 * @Description: 牌桌行为组件
 *  下的赌注；准备按钮；计时器（自己玩家自己有）
 */

const { ccclass, property, menu } = cc._decorator;

@ccclass
@menu("炸金花/BehaviorCom")
export default class ZJHBehaviorCom extends cc.Component {

    //准备完成
    @property(cc.Node)
    public readyText: cc.Node = null;
    //准备按钮
    @property(cc.Node)
    public readyBtn: cc.Node = null;
    //准备整体布局：用来显示隐藏
    @property(cc.Node)
    public readyNode: cc.Node = null;
    //是否准备好了
    public isReady: boolean = false;
    //赌注
    @property(cc.Label)
    public betCoin: cc.Label = null;
    //牌背面：三张牌
    @property(cc.Node)
    public backCard: cc.Node = null;
    //牌正面
    @property(ZJHCards)
    public zCards: ZJHCards = null;
    //牌正面节点
    @property(cc.Node)
    public zCardsNode: cc.Node = null;

    // 倒计时 node
    @property(cc.Node)
    public countdownNode: cc.Node = null;
    // 倒计时文本
    @property(cc.Label)
    public countdownLabel: cc.Label = null;
    //弃牌标志
    @property(cc.Node)
    public dropNode: cc.Node = null;
    //失败标志
    @property(cc.Node)
    public failNode: cc.Node = null;
    //看牌标志
    @property(cc.Node)
    public flipNode: cc.Node = null;

    //比牌按钮
    @property(cc.Node)
    public vsIcon: cc.Node = null;

    /**
     * 初始化
     */
    public init(isReady: boolean): void {
        this.readyNode.active = true;
        this.setReady(isReady);
        this.setBetCoin(0);
        if (this.zCardsNode) this.zCardsNode.active = false;
        this.backCard.active = false;
        if(this.flipNode) this.flipNode.active = false;
        this.failNode.active = false;
        this.dropNode.active = false;
    }

    /**
     * 初始化准备按钮
     * @param isReady 
     */
    public setReady(isReady: boolean): void {
        cc.log("zjhBehaviorCom=> isReady=" + isReady);
        this.isReady = isReady;
        this.readyText.active = isReady;
        this.readyBtn.active = !isReady;
    }

    /**
     * 点击准备按钮
     */
    public clickReady(): void {
        this.setReady(true);
    }

    public hideReadyNode(): void {
        this.readyNode.active = false;
    }
    /**
     * 设置赌注
     */
    public setBetCoin(coin: number): void {
        this.betCoin.string = coin.toFixed(0);
    }

    //设置牌背面
    public setBackCard(active: boolean) {
        this.backCard.active = active;
    }
    //设置牌正面
    public setFrontCard(active: boolean, zcards?: Array<number>) {
        this.zCardsNode.active = active;
        if (zcards) this.zCards.initCards(zcards);
    }

    /**
     * 设置倒计时
     * @param time 
     */
    public setCountdown(active: boolean, time?: number): void {
        this.countdownNode.active = active;
        if (active &&　time != null && time != undefined){
            this.countdownLabel.string = time.toString();
            if(time == 0) this.countdownNode.active = false;
        }
    }

    /**
     * 显示弃牌
     */
    public showDrop(active: boolean) {
        this.dropNode.active = active;
    }
    /**
     * 显示失败
     */
    public showFail(active: boolean) {
        this.failNode.active = active;
    }

    public showExit(state: number) {
        if (state == ZJHEndState.drop) {
            this.showDrop(true);
        } else if (state == ZJHEndState.fail) {
            this.showFail(true);
        }
    }
    
    /**
     * 显示看牌
     */
    public showFlip(active: boolean) {
        if (this.flipNode)
            this.flipNode.active = active;
    }

    /**
     * 显示选择玩家比牌按钮
     */
    public showPlayerVs(active: boolean) {
        this.vsIcon.active = active;
    }

}
