// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ZJHTop extends cc.Component {

    //单注
    @property(cc.Label)
    betUnit: cc.Label = null;

    //总注
    @property(cc.Label)
    betSum: cc.Label = null;

    @property(cc.Button)
    back: cc.Button = null;
    
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.init();
    }

    start () {

    }

    public init(){
        this.betSum.string = "0";
        this.betUnit.string = "0";
    }
    // update (dt) {}
}
