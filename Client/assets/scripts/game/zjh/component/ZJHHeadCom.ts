import { ZJHPlayer } from "../ZJHRound";

/*
 * @Author: fasthro
 * @Description: 斗地主头像组件
 * @Date: 2019-04-05 22:44:10
 */

const { ccclass, property, menu } = cc._decorator;

@ccclass
@menu("炸金花/HeadCom")
export default class ZJHHeadCom extends cc.Component {

    // 头像 Sprite
    @property(cc.Sprite)
    public headSprite: cc.Sprite = null;

    // 积分文本框
    @property(cc.Label)
    public scoreLabel: cc.Label = null;

    // name文本框
    @property(cc.Label)
    public playName: cc.Label = null;

    // 庄标识
    @property(cc.Node)
    public zNode: cc.Node = null;

    /**
     * 初始化
     */
    public init(): void {
        // TODO
        // this.headSprite.spriteFrame = null;
        this.scoreLabel.string = "0";
        //设置庄家
        this.zNode.active = false;
    }

    /**
     * 设置头像
     * @param head 
     */
    public setHead(head: string): void {
        // TODO
    }

    /**
     * 设置积分
     * @param score 
     */
    public setScore(score: number): void {
        this.scoreLabel.string = score.toString();
    }

    /**
     * 设置name
     * @param score 
     */
    public setPlayName(playName: string): void {
        this.playName.string = playName;
    }

    /**
     * 设置庄
     * @param active 
     */
    public setLord(active: boolean): void {
        this.zNode.active = active;
    }

    //设置数据
    public setData(player: ZJHPlayer): void {
        this.setHead(player.head);
        this.setPlayName(player.name);
        this.setLord(player.isLord);
        this.setScore(player.coin);
    }
}
