import ZJHHeadCom from "./ZJHHeadCom";
import ZJHController from "../../../controller/zjh/ZJHController";

const {ccclass, property, menu} = cc._decorator;

@ccclass
@menu("炸金花/headlist")
export default class ZJHHeadList extends cc.Component {

     // 头像
     @property(ZJHHeadCom)
     public headX: ZJHHeadCom = null;
     @property(ZJHHeadCom)
     public headY: ZJHHeadCom = null;
     @property(ZJHHeadCom)
     public headZ: ZJHHeadCom = null;
     @property(ZJHHeadCom)
     public headW: ZJHHeadCom = null;
     //自己的头像
     @property(ZJHHeadCom)
     public headV: ZJHHeadCom = null;
     private _controller:ZJHController;


    // onLoad () {}

    public init(controller:ZJHController):void{
        this._controller = controller;
        this.headX.init();
        this.headY.init();
        this.headZ.init();
        this.headW.init();
        this.headV.init();
    }
    
    //设置头像相关信息
    public setHeadData():void{
        this.headX.setData(this._controller.getPlayerX());
        this.headY.setData(this._controller.getPlayerY());
        this.headZ.setData(this._controller.getPlayerZ());
        this.headW.setData(this._controller.getPlayerW());
        this.headV.setData(this._controller.getPlayerV());
    }

    start () {

    }

    // update (dt) {}
}
