import ZJHCards from "../ZJHCards";
import ZJHCard from "../ZJHCard";

const { ccclass, property, menu } = cc._decorator;

@ccclass
@menu("炸金花/ResultItemCom")
export default class ZJHResultItemCom extends cc.Component {
    
    // 名称
    @property(cc.Label)
    public nameLabel: cc.Label = null;

    // 输赢
    @property(cc.Label)
    public scoreLabel: cc.Label = null;

    // 输赢赌注多少
    @property(cc.Label)
    public betLabel: cc.Label = null;

    //牌背面
    @property(cc.Node)
    mask: cc.Node = null;
    //牌正面
    @property(ZJHCards)
    card: ZJHCards = null;
    //牌正面节点
    @property(cc.Node)
    cardNode: cc.Node = null;

    //设置牌
    public setCards(showMask: boolean, cards?:Array<number>){
        this.mask.active = showMask;
        if(!showMask){
            this.cardNode.active = true;
            if(cards) this.card.initCards(cards,true);
        }
    }
}
