// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property, menu} = cc._decorator;

@ccclass
@menu("炸金花/vs")
export default class ZJHVs extends cc.Component {

    //图集
    @property(cc.SpriteAtlas)
    public atlas: cc.SpriteAtlas = null;

    //自己的头像
    @property(cc.Sprite)
    public owner: cc.Sprite = null;
    //自己比牌输了显示的图标
    @property(cc.Node)
    public ownerLose: cc.Node = null;

    //对手的头像
    @property(cc.Sprite)
    public vs: cc.Sprite = null;
    //对手比牌输了显示的图标
    @property(cc.Node)
    public vsLose: cc.Node = null;

    private _headSize: cc.Vec2 = new cc.Vec2(150, 150);
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    //显示比牌页面
    public showPopup(active: boolean, url:string = "gg", vsUrl: string = "gg"){
        if(active){
            //初始化状态
            this.ownerLose.active =false;
            this.vsLose.active = false;
            this.owner.spriteFrame = this.atlas.getSpriteFrame(url);
            this.owner.node.setContentSize(this._headSize.x, this._headSize.y);
            this.vs.spriteFrame = this.atlas.getSpriteFrame(vsUrl);
            this.vs.node.setContentSize(this._headSize.x,this._headSize.y);
        }
        this.node.active = active;
    }

    /**
     * 显示比输标志
     * @param result 比牌结果： 1 自己赢了； 0 -1 对手赢了
     */
    public showLose(result: number){
        this.ownerLose.active = result == 1 ? false: true;
        this.vsLose.active = result == 1 ? true: false;
    }

    start () {

    }

    // update (dt) {}
}
