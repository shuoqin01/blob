import ZJHBehaviorCom from "./ZJHBehaviorCom";
import ZJHController from "../../../controller/zjh/ZJHController";

const { ccclass, property, menu } = cc._decorator;

@ccclass
@menu("炸金花/behaviorlist")
export default class ZJHBehaviorList extends cc.Component {

    // 
    @property(ZJHBehaviorCom)
    public behaviorX: ZJHBehaviorCom = null;
    @property(ZJHBehaviorCom)
    public behaviorY: ZJHBehaviorCom = null;
    @property(ZJHBehaviorCom)
    public behaviorZ: ZJHBehaviorCom = null;
    @property(ZJHBehaviorCom)
    public behaviorW: ZJHBehaviorCom = null;
    @property(ZJHBehaviorCom)
    public behaviorV: ZJHBehaviorCom = null;

    private _controller: ZJHController = null;
    // onLoad () {}

    public init(controller: ZJHController): void {
        this._controller = controller;
        this.behaviorX.init(true);
        this.behaviorY.init(true);
        this.behaviorZ.init(true);
        this.behaviorW.init(true);
        this.behaviorV.init(false);//显示准备按钮
    }

    start() {

    }

    /**
     * 点击准备按钮：改变准备，隐藏；设置赌注
     */
    public clickReady(): void {
        this._controller.clickReady();
    }

    public startGame(betCoin: number): void {
        this.behaviorX.hideReadyNode();
        this.behaviorY.hideReadyNode();
        this.behaviorZ.hideReadyNode();
        this.behaviorW.hideReadyNode();
        this.behaviorV.hideReadyNode();

        this.behaviorX.setBetCoin(betCoin);
        this.behaviorY.setBetCoin(betCoin);
        this.behaviorZ.setBetCoin(betCoin);
        this.behaviorW.setBetCoin(betCoin);
        this.behaviorV.setBetCoin(betCoin);
    }
    /**
     * 设置牌
     * @param cards 
     */
    public setDeal(ownCards: Array<number>): void {
        this.behaviorX.setBackCard(true);
        this.behaviorY.setBackCard(true);
        this.behaviorZ.setBackCard(true);
        this.behaviorW.setBackCard(true);
        this.behaviorV.setBackCard(true);

        this.behaviorV.setFrontCard(false, ownCards);
    }

    /**
     * 设置比牌图标
     */
    public activeVs(active: boolean, xState:boolean, yState:boolean, zState: boolean, wState:boolean): void {
        if (xState && this.behaviorX)
            this.behaviorX.showPlayerVs(active);
        if (yState && this.behaviorY)
            this.behaviorY.showPlayerVs(active);
        if (zState && this.behaviorZ)
            this.behaviorZ.showPlayerVs(active);
        if (wState && this.behaviorW)
            this.behaviorW.showPlayerVs(active);
    }

    /**
     * 设置X选择分数
     */
    public setChoiceScoreX(): void {
        this.behaviorY.setCountdown(false);
        this.behaviorZ.setCountdown(false);
    }

    /**
     * 设置Y选择分数
     */
    public setChoiceScoreY(): void {
        this.behaviorX.setCountdown(false);
        this.behaviorZ.setCountdown(false);
    }

    // update (dt) {}
}
