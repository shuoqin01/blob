import ZJHController from "../../../controller/zjh/ZJHController";
import ZJHView from "../../../view/zjh/ZJHView";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property, menu} = cc._decorator;

@ccclass
@menu("炸金花/BottomBtn")
export default class ZJHBottomBtn extends cc.Component {

    // 比牌
    @property(cc.Button)
    public btnVs: cc.Button = null;
    // 加注
    @property(cc.Button)
    public btnAdd: cc.Button = null;
    // 看牌
    @property(cc.Button)
    public btnFlip: cc.Button = null;
    // 弃牌
    @property(cc.Button)
    public btnDrop: cc.Button = null;
    // 跟注
    @property(cc.Button)
    public btnFollow: cc.Button = null;

    //按钮的状态：保存下来是因为想在按钮执行中清除状态后，执行完成后恢复状态
    private _vsState: boolean = false;
    private _addState: boolean = false;
    private _flipState: boolean = false;
    private _dropState: boolean = false;
    private _followState: boolean = false;

    // onLoad () {}

    public init(): void{
        this.activeButtons(false);
    }
    
    public activeButtons(active: boolean, needState: boolean = true){
        if(needState){
            this._vsState = active;
            this._addState = active;
            this._flipState = active;
            this._dropState = active;
            this._followState = active;
        }
        this.btnAdd.interactable = active;
        this.btnDrop.interactable = active;
        this.btnFlip.interactable = active;
        this.btnFollow.interactable = active;
        this.btnVs.interactable = active;
    }

    //游戏开始时的状态：只有看牌和弃牌； 轮到自己出牌时可以点击跟注，加注和比牌====开始时调用
    public startState(){
        this._flipState =true;
        this._dropState = true;
        this.btnFlip.interactable = true;
        this.btnDrop.interactable = true;
    }

    //过程中的状态： 跟注、加注和比牌=====过牌后和第一次出牌调用
    public processState(active:boolean = false){
        if(active){
            this._vsState = active;
            this._addState = active;
            this._followState = active;
        }
        this.btnFollow.interactable = active;
        this.btnAdd.interactable = active;
        this.btnVs.interactable = active;
    }

    //轮到自己出牌时调用；
    public restoreState(){
        this.btnAdd.interactable = this._addState;
        this.btnDrop.interactable = this._dropState;
        this.btnFlip.interactable = this._flipState;
        this.btnFollow.interactable = this._followState;
        this.btnVs.interactable = this._vsState;
    }

    start () {

    }

    public activeFlip(active: boolean){
        this._flipState = active;
        this.btnFlip.interactable = active;
    }

    public activeAdd(active: boolean){
        this._addState = active;
        this.btnAdd.interactable = active;
    }

    // update (dt) {}
}
