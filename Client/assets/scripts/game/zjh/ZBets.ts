import ZBet from "./ZBet";
import ZJH = require("./ZJH");
import { Config } from "./ZJHRound";

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property, menu } = cc._decorator;

@ccclass
@menu("Game/ZBets")
export default class ZBets extends cc.Component {

    @property(cc.SpriteAtlas)
    public atlas: cc.SpriteAtlas = null;

    @property(cc.Prefab)
    public betPrefab: cc.Prefab = null;

    @property(cc.Vec2)
    public betSize: cc.Vec2 = new cc.Vec2(50, 50);


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {

    }

    public init(){
        this.node.removeAllChildren();
    }

    //准备完成后的图标显示
    public readyBet(betCount: number, playerNum: number) {
        for (let i = 0; i < playerNum; i++) {
            setTimeout(() => {
                this.addBet(betCount);
            }, 1000);
        }
    }

    /**
     * 添加赌注
     * @param betCount 赌注大小
     */
    public addBet(betCount: number) {
        let betNode: cc.Node = cc.instantiate(this.betPrefab);
        this.node.addChild(betNode);
        let bet: ZBet = betNode.getComponent(ZBet);
        this._initBet(bet, betCount);
    }

    //找到赌注图标，设置位置
    private _initBet(bet: ZBet, betCount: number) {
        let betCountStr = this._getBetCountStr(betCount);
        bet.node.name = betCountStr;
        bet.node.setPosition(this._getBetPosition());
        let betIcon = this.atlas.getSpriteFrame(betCountStr);
        bet.addBet(betIcon, this.betSize);
    }

    /**
     * 获取对应赌注大小图标的名称
     * @param betCount 赌注大小
     * 100 game_chip01
     * 500 game_chip03
     * 1000 game_chip05
     * 2000 game_chip07
     */
    private _getBetCountStr(betCount: number) {
        switch (betCount) {
            case Config.BET_ARRAY[0]:
                return "game_chip01";
            case Config.BET_ARRAY[1]:
                return "game_chip03";
            case Config.BET_ARRAY[2]:
                return "game_chip05";
            case 2000:
                return "game_chip07";
            default:
                return "game_chip01";
        }
    }

    //获取赌注的随机位置：
    private _getBetPosition(): cc.Vec2 {
        cc.log(" bet position x = " + this.betSize.x);
        let rBetSize = this.betSize.x / 2;
        let rWidth = this.node.getContentSize().width / 2;
        let rHeight = this.node.getContentSize().height / 2;
        let x = ZJH.Utils.random(rBetSize - rWidth, rWidth - rBetSize);
        let y = ZJH.Utils.random(rBetSize - rHeight, rHeight - rBetSize);
        cc.log("bet position, x=" + x + ",y=" + y);
        return new cc.Vec2(x, y);
    }

    // update (dt) {}
}
