import BaseGame from "./BaseGame";
import { ControllerType } from "../../define/Controllers";

/**
 * 斗地主游戏中心
 */

 export default class DDZGame extends BaseGame{

    constructor(name: string, ctrlType: ControllerType){
        super();
        this.name = name;
        this.ctrlType = ctrlType;
    }

    /**
     * game create
     */
    public static create(name: string, ctrlType: ControllerType): IGame{
        return new DDZGame(name, ctrlType);
    }

    public initialize(): void{
        super.initialize();

    }

    public onGameStarted(game: any, params: any): void{
        super.onGameStarted(game, params);

    }

    public updateGame(dt): void{

    }
    
 }