import { ControllerType } from "../../define/Controllers";

/**
 * 游戏基类
 */

export default class BaseGame implements IGame {
    //游戏名称
    public name: string = "";
    //控制器类型
    public ctrlType: ControllerType = null;
    //哪一种游戏
    public game: any = null;

    // 是否处于激活状态
    public active: boolean = false;
    // 是否可销毁
    public canDestroy: boolean = true;

    /**
     * 工厂创建游戏器,必须在子类中实现创建的方法
     * @param name 游戏名称
     */
    public static create(name: string, ctrlType: ControllerType): IGame {
        console.error(`${name} game static create method return null!`);
        return null;
    }

    /**
     * 初始化
     */
    public initialize(): void {
        this.active = false;
    }

    public update(dt: any): void {
        if(!this.active){
            return;
        }
        this.updateGame(dt);
    }

    /**
     * 更新游戏
     */
    public updateGame(dt: any){}

    /**
     * 激活游戏
     * @param game 游戏种类
     * @param params 参数
     */
    public onGameStarted(game: any, params: any): void {
        this.game = game;
        this.active = true;
    }

    public dispose(): void {
    }


}