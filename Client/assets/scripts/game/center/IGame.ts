
/**
 * 游戏接口
 */

 interface IGame{
     initialize():void;
     update(dt: any): void;
     onGameStarted(game:any, params: any): void;
     dispose():void;
 }