import BaseGame from "./BaseGame";
import { ControllerType } from "../../define/Controllers";

/**
 * 炸金花游戏中心
 */

 export default class ZJHGame extends BaseGame{

    constructor(name: string, ctrlType:ControllerType){
        super();
        this.name = name;
    }

    /**
     * game create
     */
    public static create(name: string, ctrlType:ControllerType): IGame{
        return new ZJHGame(name, ctrlType);
    }

    public initialize(): void{
        super.initialize();

    }

    public onGameStarted(game: any, params: any): void{
        super.onGameStarted(game, params);

    }

    public updateGame(dt): void{

    }
    
 }