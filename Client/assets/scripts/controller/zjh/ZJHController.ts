import BaseController from "../BaseController";
import ZJHView from "../../view/zjh/ZJHView";
import { ZJHRound, ZJHEvent, ZJHEventData, ZJHPlayer, ZJHEndState, Config } from "../../game/zjh/ZJHRound";
import { UILayer } from "../../define/UILayer";
import Game from "../../Game";
import { ControllerType } from "../../define/Controllers";
import ZJH = require("../../game/zjh/ZJH");

export default class ZJHController extends BaseController {
    // view
    private _view: ZJHView = null;
    // round
    private _round: ZJHRound = null;
    public get round(): ZJHRound {
        return this._round;
    }

    //自动出牌加注的赌注索引
    private _index: number = 0;

    /**
     * controller create
     * @param name 
     */
    public static create(name: string): IController {
        return new ZJHController();
    }

    public initialize(): void {
        super.initialize();
        this.layer = UILayer.Window;
    }

    public onViewCreated(go: any, params: any): void {
        super.onViewCreated(go, params);
        this._view = this.gameObject.addComponent(ZJHView);
        this._view.initView(this);

        // 创建 Round: 比赛 中止和 结束
        let breakEvent: ZJHEvent = { name: "_onBreakEvent", handler: this._onBreakEvent, context: this };
        let overEvent: ZJHEvent = { name: "_onOverEvent", handler: this._onOverEvent, context: this };
        // 创建 Round
        this._round = new ZJHRound(100, breakEvent, overEvent);

        //初始化数据
        this.setInitData();

        // 绑定事件
        // 准备
        this._round.bindReadyEvent({ name: "_onReadyEvent", handler: this._onReadyEvent, context: this });
        // 发牌
        this._round.bindDealEvent({ name: "_onDealEvent", handler: this._onDealEvent, context: this });
        //跟注
        this._round.bindFollowEvent({ name: "_onFollowEvent", handler: this._onFollowEvent, context: this });
        //加注
        this._round.bindAddEvent({ name: "_onAddEvent", handler: this._onAddEvent, context: this });
        //弃牌
        this._round.bindDropEvent({ name: "_onDropEvent", handler: this._onDropEvent, context: this });
        //看牌
        this._round.bindFlipEvent({ name: "_onFlipEvent", handler: this._onFlipEvent, context: this });
        //比牌
        this._round.bindVsEvent({ name: "_onVSEvent", handler: this._onVSEvent, context: this });

        // 产生庄
        this._round.bindCreateLordEvent({ name: "_onCreateLordEvent", handler: this._onCreateLordEvent, context: this });

        // 倒计时
        this._round.bindTimeoutEvent({ name: "_onTimeoutEvent", handler: this._onTimeoutEvent, context: this });

        // Round
        this._round.init();
    }

    /**
     * 重新开始游戏
     */
    public restart(): void {
        //round数据
        this._round.init();
        // 初始化 ui
        this._view.initView(this);
        //头像信息
        this.setInitData();
    }

    /**
     * 比赛中止; 弃牌0 drop，比牌失败1 fail
     * @param data 
     */
    private _onBreakEvent(data: ZJHEventData): void {
        this._stopPlay(data);
    }

    //玩家比赛结束
    private _stopPlay(data: ZJHEventData) {
        switch (data.player.id) {
            case this.getPlayerX().id:
                this._view.behaviorList.behaviorX.showExit(data.endstate);
                this._round.playerX.state = false;
                break;
            case this.getPlayerY().id:
                this._view.behaviorList.behaviorY.showExit(data.endstate);
                this._round.playerY.state = false;
                break;
            case this.getPlayerZ().id:
                this._view.behaviorList.behaviorZ.showExit(data.endstate);
                this._round.playerZ.state = false;
                break;
            case this.getPlayerW().id:
                this._view.behaviorList.behaviorW.showExit(data.endstate);
                this._round.playerW.state = false;
                break;
            case this.getPlayerV().id:
            default:
                this._view.behaviorList.behaviorV.showExit(data.endstate);
                this._round.playerV.state = false;
                break;
        }
        if(data.player.owner) this._view.bottomBtn.activeButtons(false);
    }

    /**
     * 比赛结束
     * @param data 
     */
    private _onOverEvent(data: ZJHEventData): void {
        cc.log("游戏结束 ： winner is "+ data.player.name);
        //显示页面
        Game.showUI(ControllerType.ZJHResult, this._round);
        //状态重置
        setTimeout(() => {
            this.restart();
        }, Config.RESULT_SHOW_TIME);
    }

    /**
     * 初始化数据
     */
    private setInitData(): void {
        this._view.headList.setHeadData();
    }

    public clickReady() {
        this._round.ready();
    }

    public getPlayerX(): ZJHPlayer {
        return this._round.playerX;
    }
    public getPlayerY(): ZJHPlayer {
        return this._round.playerY;
    }
    public getPlayerZ(): ZJHPlayer {
        return this._round.playerZ;
    }
    public getPlayerW(): ZJHPlayer {
        return this._round.playerW;
    }
    public getPlayerV(): ZJHPlayer {
        return this._round.playerV;
    }

    /**
     * 准备:所有角色都会绑定，但是只有操作者会执行
     * @todo 这里的准备是玩家自己用的
     */
    private _onReadyEvent(data: ZJHEventData): void {
        this._view.behaviorList.behaviorV.setReady(true);


        //隐藏按钮，发牌;显示计时器
        setTimeout(() => {
            //隐藏按钮；设置赌注
            this._view.startGame(this._round.score, this._round.sum);
            //发牌
            this._round.deal();
            //显示赌注图标
            this._view.zBets.readyBet(Config.BET_ARRAY[0], this._round.playerNum);
            //底部按钮可以点击；显示倒计时
            // this._view.bottomBtn.activeButtons(true);
            this._view.bottomBtn.startState();
            //显示庄标志
            this._round.createRandomLord();
            this._round.first.choiceTimeout();
        }, 1000);

    }

    /**
     * 发牌
     * @param data 
     */
    private _onDealEvent(data: ZJHEventData): void {
        //显示牌背面
        this._view.behaviorList.setDeal(data.cards);
        cc.log("自己的牌： " + data.cards.toString());
    }

    /**
     * 跟注
     * @param data 
     */
    private _onFollowEvent(data: ZJHEventData): void {
        this._onTimeoutEvent(data,false);
        this.updateShowBet(data.player.score, data);
        this._view.zBets.addBet(data.player.score);

        this._toNextPlayer(data);
    }

    /**
     * 加注
     * @param data 
     */
    private _onAddEvent(data: ZJHEventData): void {
        this._onTimeoutEvent(data,false);
        this.updateShowBet(data.player.score, data);
        this._view.zBets.addBet(data.player.score);
        this._toNextPlayer(data);
    }

    //下一个玩家开始
    private _toNextPlayer(data: ZJHEventData) {
        if(data.player.owner)
            this._view.bottomBtn.processState();
        //关闭计时器，在操作开始的时候关闭
        // this._onTimeoutEvent(data, false);
        //找到下一个玩家
        let player = this._round.findNextPlayer();
        //打开计时器
        player.choiceTimeout();
        //开始自动出牌
        let next = new ZJHEventData();
        next.player = player;
        this._auto(next);
        if(player.owner){
            if(this._round.loop == 1) this._view.bottomBtn.processState(true);
            else this._view.bottomBtn.restoreState();
        }
    }

    /**
     * 显示更新赌注相关的数据：顶部单注，总注；玩家总下注，玩家游戏币
     * @param betCoin 更新顶部赌注
     * @param data 
     */
    private updateShowBet(betCoin: number, data: ZJHEventData) {
        //更新玩家赌注
        if (this._round.isPlayerX(data.player.id)) {
            this._view.headList.headX.setScore(data.player.coin);
            this._view.behaviorList.behaviorX.setBetCoin(data.player.sum);
        } else if (this._round.isPlayerY(data.player.id)) {
            this._view.headList.headY.setScore(data.player.coin);
            this._view.behaviorList.behaviorY.setBetCoin(data.player.sum);
        } else if (this._round.isPlayerZ(data.player.id)) {
            this._view.headList.headZ.setScore(data.player.coin);
            this._view.behaviorList.behaviorZ.setBetCoin(data.player.sum);
        } else if (this._round.isPlayerW(data.player.id)) {
            this._view.headList.headW.setScore(data.player.coin);
            this._view.behaviorList.behaviorW.setBetCoin(data.player.sum);
        } else {
            this._view.headList.headV.setScore(data.player.coin);
            this._view.behaviorList.behaviorV.setBetCoin(data.player.sum);
        }
        //更新顶部赌注,单注数据在player里更新
        this._round.sum += betCoin;
        this._view.setTop(this._round.score, this._round.sum);
    }

    //加注之前，先判断哪些赌注是可以添加的
    public preAdd() {
        let score = this._round.score;
        if (score >= Config.BET_ARRAY[0])
            this._view.oneBet.interactable = false;
        if (score >= Config.BET_ARRAY[1])
            this._view.fiveBet.interactable = false;
        if (score >= Config.BET_ARRAY[2])
            this._view.tenBet.interactable = false;
        this._round.playerV.clearTimeout();
        this._view.setTimeoutV(0);
    }

    //比牌之前
    public preVs(){
        this._round.playerV.clearTimeout();
        this._view.setTimeoutV(0);
    }
    /**
     * 看牌
     * @param data 
     */
    private _onFlipEvent(data: ZJHEventData): void {
        //显示看牌标识；操作者显示牌
        if (data.player.owner) {
            // this._view.behaviorList.behaviorV.setBetCoin(data.player.sum);
            this._view.bottomBtn.activeFlip(false);
            this._view.behaviorList.behaviorV.setFrontCard(true);
            this._view.behaviorList.behaviorV.setBackCard(false);
        } else {
            if (this._round.isPlayerX(data.player.id)) {
                this._view.behaviorList.behaviorX.showFlip(true);
                // this._view.behaviorList.behaviorX.setBetCoin(data.player.sum);
            } else if (this._round.isPlayerY(data.player.id)) {
                this._view.behaviorList.behaviorY.showFlip(true);
                // this._view.behaviorList.behaviorY.setBetCoin(data.player.sum);
            } else if (this._round.isPlayerZ(data.player.id)) {
                this._view.behaviorList.behaviorZ.showFlip(true);
                // this._view.behaviorList.behaviorZ.setBetCoin(data.player.sum);
            } else if (this._round.isPlayerW(data.player.id)) {
                this._view.behaviorList.behaviorW.showFlip(true);
                // this._view.behaviorList.behaviorW.setBetCoin(data.player.sum);
            }
        }
    }
    /**
     * 弃牌
     * @param data 
     */
    private _onDropEvent(data: ZJHEventData): void {
        this._onTimeoutEvent(data,false);
        //显示弃牌标识，只有一个人时显示结束页面
        if (data.player.owner) {
            this._view.bottomBtn.activeButtons(false);
        }
        data.endstate = ZJHEndState.drop;
        this._stopPlay(data);//显示弃牌的标志

        if (this._round.isGameOver()) {
            this._round.overRound(data);
        }else if(data.player.id == this._round.current.id){//当前玩家弃牌
            this._toNextPlayer(data);
        }
    }
    /**
     * 比牌
     * @param data 
     */
    private _onVSEvent(data: ZJHEventData): void {
        this._onTimeoutEvent(data,false);
        let pickPlayer: ZJHPlayer;
        if (this._round.isPlayerX(data.pickId)) pickPlayer = this.getPlayerX();
        else if (this._round.isPlayerY(data.pickId)) pickPlayer = this.getPlayerY();
        else if (this._round.isPlayerZ(data.pickId)) pickPlayer = this.getPlayerZ();
        else if (this._round.isPlayerW(data.pickId)) pickPlayer = this.getPlayerW();
        else if (this._round.isPlayerV(data.pickId)) pickPlayer = this.getPlayerV();

        //更新赌注
        this.updateShowBet(this._round.score * 2, data);
        //加载比牌页面
        this._view.vsPopup.showPopup(true, data.player.head, pickPlayer.head);
        cc.log(`比牌 ==> 自己的牌 ${ZJH.Core.getCardsType(data.player.cards)}; 对手的牌 ${ZJH.Core.getCardsType(pickPlayer.cards)}`)
        //比较牌的大小
        let result = ZJH.Core.compareCards(data.player.cards, pickPlayer.cards);

        setTimeout(() => {
            //显示失败图标
            this._view.vsPopup.showLose(result);
            //处理失败的数据:状态
            this._round.executeBreakEvent(result==1 ?pickPlayer:data.player, ZJHEndState.fail);
            this._round.playerNum -= 1;
            cc.log(`player number=${this._round.playerNum}, is game over=${this._round.isGameOver()}`);
            if (this._round.isGameOver()) {
                this._round.overRound(data);
            }else{
                if (result == 1) {//赢了
                    data.player.follow();
                } else {//输了
                    this._toNextPlayer(data);
                }
            }

            setTimeout(() => {
                this._view.vsPopup.showPopup(false);

            }, 1000);
        }, 1000);
    }

    /**
     * 设置庄家,设置完成后，开始自动出牌
     * @param data 
     */
    private _onCreateLordEvent(data: ZJHEventData): void {
        // 设置标志
        if (this._round.isPlayerX(data.player.id)) this._view.setCreateLordX();
        else if (this._round.isPlayerY(data.player.id)) this._view.setCreateLordY();
        else if (this._round.isPlayerZ(data.player.id)) this._view.setCreateLordZ();
        else if (this._round.isPlayerW(data.player.id)) this._view.setCreateLordW();
        else this._view.setCreateLordV();

        //如果是操作者，显示需要的按钮
        if(data.player.owner){
            this._view.bottomBtn.processState(true);
        }
        this._auto(data);
    }

    /**
     * 自动出牌
     * @param data 
     */
    private _auto(data: ZJHEventData) {
        if (data.player.owner || !data.player.agent) return;

        switch (this._round.loop) {
            case 1://loop == 1 跟注，加注
                setTimeout(() => {
                    if (ZJH.Utils.random(0, 1))
                        data.player.follow();
                    else {
                        if (data.player.getBetIndex() >= Config.BET_ARRAY.length - 1) return;
                        data.player.add(data.player.getBetIndex()+1);
                    }
                }, Config.AGENT_TIME);
                break;
            case 2://loop == 2 看牌,跟注
                setTimeout(() => {
                    if(data.player.isFlip){
                        this._onFlipEvent(data);
                        return;
                    }
                    data.player.flip();
                    data.player.follow();
                }, Config.AGENT_TIME);
                break;
            case 3://loop == 3 比牌
            default:
                setTimeout(() => {
                    this._round.createRandomLord
                    data.player.pickVs(this._round.randomVsPlayer(data.player.id));
                }, Config.AGENT_TIME);
                break;
        }
    }

    /**
    * 倒计时
    * @param data 
    */
    private _onTimeoutEvent(data: ZJHEventData, active: boolean = true): void {
        if (this._round.isPlayerX(data.player.id)) this._view.behaviorList.behaviorX.setCountdown(active, data.timeout);
        else if (this._round.isPlayerY(data.player.id)) this._view.behaviorList.behaviorY.setCountdown(active, data.timeout);
        else if (this._round.isPlayerZ(data.player.id)) this._view.behaviorList.behaviorZ.setCountdown(active, data.timeout);
        else if (this._round.isPlayerW(data.player.id)) this._view.behaviorList.behaviorW.setCountdown(active, data.timeout);
        else this._view.behaviorList.behaviorV.setCountdown(active, data.timeout);

        if(data.timeout <= 0) this._toNextPlayer(data);
    }

    public update(dt): void {
        if (!this.active)
            return;
    }

    public getResPath(): string {
        return "prefabs/ui/zjh/zjh_view";
    }
}
