import BaseController from "../BaseController";
import { UILayer } from "../../define/UILayer";
import ZJHResultView from "../../view/zjh/ZJHResultView";
import { ZJHRound,Config, ZJHEventData } from "../../game/zjh/ZJHRound";
import { ControllerType } from "../../define/Controllers";
import Game from "../../Game";

/*
 * @Author: fasthro
 * @Description: 结果
 * @Date: 2019-04-08 17:45:18
 */

export default class ZJHResultController extends BaseController {
    // view
    private _view: ZJHResultView = null;

    /**
     * controller create
     * @param name 
     */
    public static create(name: string): IController {
        return new ZJHResultController();
    }

    public getResPath(): string {
        return "prefabs/ui/zjh/ZJHResult_view";
    }

    public initialize(): void {
        super.initialize();
        this.layer = UILayer.Window;
    }

    public onViewCreated(go: any, params: any): void {
        super.onViewCreated(go, params);
        this._view = this.gameObject.getComponent(ZJHResultView);

        let data = <ZJHRound>params;

        this._view.winNode.active = data.playerV.state;
        this._view.loseNode.active = !data.playerV.state;

        this._view.setItemV(data.playerV, data.sum);
        this._view.setItemX(data.playerX, data.sum);
        this._view.setItemY(data.playerY, data.sum);
        this._view.setItemZ(data.playerZ, data.sum);
        this._view.setItemW(data.playerW, data.sum);

        setTimeout(() => {
            Game.closeUI(ControllerType.ZJHResult);
        }, Config.RESULT_SHOW_TIME);
    }
}
